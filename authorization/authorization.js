
const allowedUsers = {
    AMakus: 'kS8qZPkQhSQasAfw',
    VSergeev: 'edQdRNN82xAdJddC',
    Okucherenko: '92bK9b4BNNG8Amz3'
}
const employeeQuery = window.location.search.substring(1);
const allowedKeys = Object.values(allowedUsers);

if (!allowedKeys.includes(employeeQuery) && !window.location.href.includes('forbidden')){
    window.location.replace('https://candidate-qa.platform500.com/forbidden.html')
}



