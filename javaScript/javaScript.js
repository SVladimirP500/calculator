const textView = document.form.textview;
const clearState = {
    count: 0,
    lastNumber: 0
};



//ввод значений
function insert (num) {
    if (clearState.lastNumber > 0){
        textView.value = clearState.lastNumber + num;
        clearState.lastNumber = 0
    }else {
        textView.value += num;
    }
}
//Отчистить

function clean(){
    const arrClear = textView.value
    clearState.count++
    switch (clearState.count){
        case 1:
            clearState.lastNumber = arrClear.slice(arrClear.split('').length-1);
            textView.value = "";
        case 2:
            textView.value = "";
            clearState.count = 0;
    }
}

//удалить один символ
function back(){
    textView.value = textView.value.substring(0, textView.value.length-1);
}
//Вычисления результата
function equal() {

    const str = textView.value;
    const expNull = str.match(/\*0/);
    const arrOperator = str.split(/[0-9]/).join('').trim().split('')
    const arrNumber = str.split(/[+*/-]/).map(item => Number(item))

    function calculation (arr){
        const res = arr.reduce((total, item, index) => {
            if(total === 0){
                return item;
            }else if (arrOperator[index - 1]) {
                return eval(`${total}` + `${arrOperator[index-1]}` + `${item}`);
            }
            return total
        }, 0);
        textView.value = res
    }

    if (expNull === null && arrOperator.length < 4) {
        calculation(arrNumber)
    }
    else if(expNull === null && arrOperator.length > 3) {
        const arrnumberLimit = arrNumber.slice(0,3)
        calculation(arrnumberLimit)
    }
    else {
        textView.value = "";
    }

}

//Сценарий с обновлением страници
if(performance.navigation.type == 1){
    alert('Progres can lost after loads the page ')
}

